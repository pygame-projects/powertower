from numpy.linalg import norm
import pygame

background_img_size = 64


class Projectile:
    """A class which represents projectiles summoned by ranged attacks"""
    def __init__(self, x, y, direction_vector, speed, power, effect_area, opponents):
        """Create a Projectile

        x -- integer for initial x coordinate
        y -- integer for initial y coordinate
        direction_vector -- list of integers for the direction vector to follow
        speed -- integer for the speed of the projectile
        power -- integer for the damages caused by the projectile
        effect_area -- integer for the range of the damages when the projectile hits
        opponents -- list of Unit, potential targets for the projectile"""
        self.power = power
        self.effect_area = effect_area
        self.opponents = opponents
        vector_norm = norm(direction_vector)
        self.speed_vector = [int(speed * direction_vector[0]/vector_norm), int(speed * direction_vector[1]/vector_norm)]
        self.x = x
        self.y = y
        self.has_hit = False

    def damage(self, unit):
        """Apply the attack to an unit

        unit -- the Unit to be damaged"""
        unit.current_health = max(0, unit.current_health - self.power)

    def move(self):
        """Move the projectile, by following the direction vector"""
        self.x += self.speed_vector[0]
        self.y += self.speed_vector[1]
        i = 0
        while not self.has_hit and i < len(self.opponents) :
            unit = self.opponents[i]
            if unit.x - background_img_size//2 < self.x < unit.x + background_img_size//2 \
                and unit.y - background_img_size//2 < self.y < unit.y + background_img_size//2:
                self.has_hit =True
            i += 1
        if self.has_hit :
            for unit in self.opponents:
                if abs(unit.x - self.x) <= self.effect_area and abs(unit.y - self.y) <= self.effect_area:
                    self.damage(unit)

    def draw(self, frame):
        """Draw the projectile on the window

        frame -- pygame.Surface, the window to draw on"""
        pygame.draw.circle(frame, (255, 140, 0), (self.x, self.y), 10)
