import pygame
from state import State


class Player:
    """A class for the player and its team"""
    def __init__(self):
        """Create a player, give empty team"""
        self.team = []
        self.alives = []
        self.exp = 0

    def add_unit(self, unit):
        """Add an unit to the player team and its alive units

        unit -- the Unit to add"""
        self.team.append(unit)
        self.alives.append(unit)

    def check_death(self):
        """Remove dead ally units from the team"""
        for unit in self.alives :
            if unit.current_health <= 0 :
                unit.x = -1
                unit.y =  -1 # prevent dead people from stopping projectiles
        self.alives = [unit for unit in self.alives if unit.current_health > 0]

    def has_lost(self):
        """Check if player has remaining alive units"""
        return len(self.alives) == 0

    def reset(self):
        """Reset the player state between stages"""
        for unit in self.team:
            unit.state = State.IDLE
            unit.current_images_index = 0
            unit.animation_count = 0
            unit.current_health = unit.max_health
            unit.path.clear()
            unit.destination = []
            unit.attack.count = 0

        self.alives = []
