

class Attack:
    """Generic class for the attack behaviour"""
    def __init__(self, power, reach, effect_area, level, cooldown):
        """Instantiate an attack

        power -- an integer for the damages of the attack
        reach -- an integer for the range of the trigger of the attack
        effect_area -- an integer for the range of the damages when the attack hits
        level -- an integer for the level of the attack
        cooldown -- an integer which represents the number of frames to wait before re-attack

        count is an integer attribute which handles the attack cooldown"""
        self.power = power
        self.reach = reach
        self.effect_area = effect_area
        self.level = level
        self.cooldown = cooldown
        self.count = 0

    def damage(self, unit):
        """Apply the attack to an unit

        unit -- the Unit to be damaged"""
        unit.current_health = max(unit.current_health - self.power, 0)

    def hit(self, attacker_x, attacker_y, aimed_x, aimed_y, unit_list, projectiles):
        """Trigger the attack and check if someone is hit

        attacker_x -- an integer for the x coordinate of the attacker
        attacker_y -- an integer for the y coordinate of the attacker
        aimed_x -- an integer for the x coordinate of the aimed point
        aimed_x -- an integer for the y coordinate of the aimed point
        unit_list -- a list of Unit,  potential targets for the attack
        projectiles -- list of Projectile"""
        for unit in unit_list:
            if abs(unit.x - aimed_x) < self.effect_area and abs(unit.y - aimed_y) < self.effect_area:
                self.damage(unit)
                self.count = self.cooldown

    def level_up(self):
        pass
