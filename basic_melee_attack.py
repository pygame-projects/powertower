from attack import Attack

base_range = 32
base_power = 3
base_area = 32
base_cooldown = 60


class BasicMeleeAttack(Attack):
    """A regular type of melee attack"""

    def __init__(self, level):
        """Creates a basic melee attack instance
        level -- the level of the attack"""
        super().__init__(base_power, base_range, base_area, level, base_cooldown)

    def level_up(self):
        """Increase the level of the attack"""
        self.level += 1
