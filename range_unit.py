import pygame
import os
from unit import Unit
from basic_range_attack import BasicRangeAttack
from direction import Direction
from state import State

background_img_size = 64

images = {(State.IDLE, Direction.RIGHT): [pygame.transform.scale(
            pygame.image.load(os.path.join("assets/used/range_unit/wizzard_f_idle_anim_f" + str(i) + ".png")),
            (background_img_size, background_img_size)) for i in range(4)],
          (State.IDLE, Direction.LEFT): [pygame.transform.flip(pygame.transform.scale(
            pygame.image.load(os.path.join("assets/used/range_unit/wizzard_f_idle_anim_f" + str(i) + ".png")),
            (background_img_size, background_img_size)), True, False) for i in range(4)],
          (State.RUN, Direction.RIGHT): [pygame.transform.scale(
            pygame.image.load(os.path.join("assets/used/range_unit/wizzard_f_idle_anim_f" + str(i) + ".png")),
            (background_img_size, background_img_size)) for i in range(4)],
          (State.RUN, Direction.LEFT): [pygame.transform.flip(pygame.transform.scale(
            pygame.image.load(os.path.join("assets/used/range_unit/wizzard_f_idle_anim_f" + str(i) +".png")),
            (background_img_size, background_img_size)), True, False) for i in range(4)],
          # TODO animations à changer
          (State.ATTACK, Direction.RIGHT): [pygame.transform.scale(
            pygame.image.load(os.path.join("assets/used/range_unit/wizzard_f_idle_anim_f" + str(i) + ".png")),
            (background_img_size, background_img_size)) for i in range(4)],
          (State.ATTACK, Direction.LEFT): [pygame.transform.flip(pygame.transform.scale(
            pygame.image.load(os.path.join("assets/used/range_unit/wizzard_f_idle_anim_f" + str(i) + ".png")),
            (background_img_size, background_img_size)), True, False) for i in range(4)],
          }

base_health = 5
base_speed = 4


class RangeUnit(Unit):
    """Wizard, polyvalent ally ranged unit"""

    def __init__(self, initial_x, initial_y, level):
        """Creates a MeleeUnit

        initial_x -- integer for the initial x coordinate
        initial_y -- integer for the initial y coordinate
        level -- integer for the unit level"""
        super().__init__(initial_x, initial_y, base_speed, BasicRangeAttack(level), base_health)
        self.level = level
        self.images = dict(images)

    def level_up(self):
        """Increase the level of the unit"""
        self.attack.level_up()
        self.level += 1
