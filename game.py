import pygame
import os
from melee_unit import MeleeUnit
from lizard_unit import LizardUnit
from range_unit import RangeUnit
from stage import Stage
from tower import Tower
from player import Player

background_img_size = 64


class Game:
    """Class which handles navigation between the different stages of the tower"""
    def __init__(self):
        """Instantiate a Game"""
        pygame.init()
        self.width = 1300
        self.height = 700
        self.win = pygame.display.set_mode((self.width, self.height))
        pygame.display.set_caption("Powertower")
        self.tower = Tower(os.path.join("towers/testTower/tower"))
        self.stage = self.tower.get_current_stage()
        self.player = Player()
        self.player.add_unit(MeleeUnit(100, 200, 1))
        self.player.add_unit(RangeUnit(100, 200, 1))
        self.player.add_unit(LizardUnit(100, 200, 1))
        self.font = pygame.font.Font(pygame.font.get_default_font(), 35)

    def draw(self):
        """Draw the game"""
        self.win.fill((0, 0, 0))
        pygame.display.update()

    def play(self):
        """Launch the tower, navigate between stages"""
        tower_finished = False
        interrupted = False
        while not interrupted and not tower_finished and not self.player.has_lost():
            self.draw()
            self.player.reset()
            if not self.stage.initial_phase(self.player, self.win, self.font, self.tower.current_stage_index + 1):
                interrupted = self.stage.run_battle(self.player, self.win, self.font, self.tower.current_stage_index + 1)
                if self.stage.is_complete():
                    if self.tower.has_next_stage():
                        self.tower.go_to_next_stage()
                        self.stage = self.tower.get_current_stage()
                    else :
                        tower_finished = True
            else:
                interrupted = True

        pygame.quit()


# main program
game = Game()
game.play()
