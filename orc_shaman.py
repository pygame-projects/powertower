import pygame
import os
from unit import Unit
from basic_range_attack import BasicRangeAttack
from direction import Direction
from state import State

background_img_size = 64

images = {(State.IDLE, Direction.RIGHT): [pygame.transform.scale(
            pygame.image.load(os.path.join("assets/used/orc_shaman/orc_shaman_idle_anim_f" + str(i) + ".png")),
            (background_img_size, background_img_size)) for i in range(4)],
          (State.IDLE, Direction.LEFT): [pygame.transform.flip(pygame.transform.scale(
            pygame.image.load(os.path.join("assets/used/orc_shaman/orc_shaman_idle_anim_f" + str(i) + ".png")),
            (background_img_size, background_img_size)), True, False) for i in range(4)],
          (State.RUN, Direction.RIGHT): [pygame.transform.scale(
            pygame.image.load(os.path.join("assets/used/orc_shaman/orc_shaman_idle_anim_f" + str(i) + ".png")),
            (background_img_size, background_img_size)) for i in range(4)],
          (State.RUN, Direction.LEFT): [pygame.transform.flip(pygame.transform.scale(
            pygame.image.load(os.path.join("assets/used/orc_shaman/orc_shaman_idle_anim_f" + str(i) +".png")),
            (background_img_size, background_img_size)), True, False) for i in range(4)],
          (State.ATTACK, Direction.RIGHT): [pygame.transform.scale(
            pygame.image.load(os.path.join("assets/used/orc_shaman/orc_shaman_attack_anim_f" + str(i) + ".png")),
            (background_img_size, background_img_size)) for i in range(4)],
          (State.ATTACK, Direction.LEFT): [pygame.transform.flip(pygame.transform.scale(
            pygame.image.load(os.path.join("assets/used/orc_shaman/orc_shaman_attack_anim_f" + str(i) + ".png")),
            (background_img_size, background_img_size)), True, False) for i in range(4)]
          }

base_health = 4
base_speed = 2


class OrcShaman(Unit):
    """Orc shaman, polyvalent ranged monster"""

    def __init__(self, initial_x, initial_y, level) :
        super().__init__(initial_x, initial_y, base_speed, BasicRangeAttack(level), base_health)
        self.level = level
        self.images = dict(images)
