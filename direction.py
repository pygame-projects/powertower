from enum import Enum


class Direction(Enum):
    """Enum for the different possible directions"""
    UP = 1
    DOWN = 2
    LEFT = 3
    RIGHT = 4
