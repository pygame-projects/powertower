import pygame
import os
from queue import Queue
from math import inf

from tile import Tile

background_img_size = 64
floor_image = pygame.transform.scale(
    pygame.image.load(
        os.path.join("assets/0x72_DungeonTilesetII_v1.3.1/0x72_DungeonTilesetII_v1.3.1/frames/floor_1.png")),
    (background_img_size, background_img_size))

wall_image = pygame.transform.scale(
    pygame.image.load(
        os.path.join(
            "assets/0x72_DungeonTilesetII_v1.3.1/0x72_DungeonTilesetII_v1.3.1/frames/wall_corner_front_left.png")),
    (background_img_size, background_img_size))


class Board:
    """A class which represents the map, without enemies"""
    def __init__(self, file_name):
        """Creates a board from a file

        file_name -- a string for the name of the file to open"""
        with open(file_name, "r") as file:
            [self.grid_width, self.grid_height] = [int(j) for j in file.readline().split()]
            self.grid = [[Tile.FLOOR for y in range(self.grid_height)] for x in range(self.grid_width)]
            for y in range(self.grid_height):
                line = file.readline().rstrip("\n")
                for x,char in enumerate(line):
                    if char == 'f':
                        self.grid[x][y] = Tile.FLOOR
                    elif char == "w":
                        self.grid[x][y] = Tile.WALL

    def pixels_to_tile(self, x, y):
        """Convert x,y pixels coordinates to coordinates on the grid

        x -- an integer for the x pixel coordinate
        y -- an integer for the y pixel coordinate"""
        return [x // background_img_size, y // background_img_size]

    def is_tile_wall(self, x, y):
        """Tell if a (x, y) tile is a wall

        x -- an integer for the x grid coordinate
        y -- an integer for the x grid coordinate"""
        return self.grid[x][y] == Tile.WALL

    def is_wall(self, x, y):
        """Tell if a (x, y) pixel position is a wall

        x -- an integer for the x pixel coordinate to check
        y -- an integer for the y pixel coordinate to check"""
        # TODO : to adapt if the grid does not start at (0,0)
        return self.is_tile_wall(self.pixels_to_tile(x, y)[0], self.pixels_to_tile(x, y)[1])

    def get_path(self, parent_array, start, goal):
        """Get the path from the array which contains parent of every node in the path

        parent_array -- list of lists of integer coordinates which contains parents
        start -- list of integer coordinates for the path start
        goal -- list of integer coordinates for the path goal"""
        temp = goal
        path = [goal]
        while temp is not None and temp != start:
            temp = parent_array[temp[0]][temp[1]]
            if temp != start:
                path.insert(0, temp)
        return path

    def path_grid_to_pixels(self, path):
        """Convert each step of a path in grid coordinates to pixel coordinates

        path -- list of lists of integer grid coordinates"""
        path = [step for step in path if step is not None]
        return list(map(lambda step: [step[0] * background_img_size + background_img_size//2,
                    step[1] * background_img_size + background_img_size//2], path))

    def shortest_path(self, start, goal):
        """Use BFS algorithm to compute the shortest path between two points

        start -- list of integer coordinates for the path start
        goal -- list of integer coordinates for the path goal"""
        dx = [1, -1, 0, 0, 1, -1, 1, -1]
        dy = [0, 0, 1, -1, 1, -1, -1, 1]
        visited = [[False for y in range(self.grid_height)] for x in range(self.grid_width)]

        level = [[inf for y in range(self.grid_height)] for x in range(self.grid_width)]

        parent = [[None for y in range(self.grid_height)] for x in range(self.grid_width)]

        visited[start[0]][start[1]] = True
        level[start[0]][start[1]] = 0
        q = Queue()
        q.put(start)
        m = len(dx)
        while not q.empty() and not visited[goal[0]][goal[1]]:
            top = q.get()
            for i in range(m):
                temp = [top[0] + dx[i], top[1] + dy[i]]
                if 0 <= temp[0] < self.grid_width and 0 <= temp[1] < self.grid_height \
                    and not visited[temp[0]][temp[1]] and not self.is_tile_wall(temp[0], temp[1]):
                    # if this is a diagonal move, we have to check walls
                    if abs(dx[i]) + abs(dy[i]) < 2 or (not self.is_tile_wall(top[0] + dx[i], top[1])
                    and not self.is_tile_wall(top[0], top[1] + dy[i])):
                        visited[temp[0]][temp[1]] = True
                        level[temp[0]][temp[1]] = level[top[0]][top[1]] + 1
                        parent[temp[0]][temp[1]] = top
                        q.put(temp)

        return self.path_grid_to_pixels(self.get_path(parent, start, goal))

    def draw(self, frame):
        """Draw the board

        frame -- pygame.Surface, the window to draw on"""
        for y in range(0, self.grid_height):
            for x in range(0, self.grid_width):
                if (self.grid[x][y] == Tile.FLOOR):
                    frame.blit(floor_image, (x * background_img_size, y * background_img_size))
                elif (self.grid[x][y] == Tile.WALL):
                    frame.blit(wall_image, (x * background_img_size, y * background_img_size))
