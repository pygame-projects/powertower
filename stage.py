import pygame
from board import Board
from orc_shaman import OrcShaman
from goblin import Goblin
from state import State
from demon import Demon
from direction import Direction
from tile import Tile

background_img_size = 64
LEFT_CLICK = 1
RIGHT_CLICK = 3

monsters_map = {"goblin": Goblin,
                "orc_shaman": OrcShaman,
                "demon": Demon}


class Stage:
    """Class which represents a stage, which is a board with enemies"""
    def __init__(self, file_name):
        """Creates a stage from a file

        file_name -- a string for the name of the file to open"""
        self.projectiles = []
        with open(file_name, "r") as file:
            board_file = file.readline()
            self.board = Board(board_file.rstrip("\n"))
            nb_enemies = int(file.readline())
            self.enemies = []
            for i in range(nb_enemies):
                [monster_type, level, grid_x, grid_y] = file.readline().split()
                x = int(grid_x)*background_img_size + background_img_size//2
                y = int(grid_y)*background_img_size + background_img_size//2
                if monster_type in monsters_map.keys():
                    self.enemies.append(monsters_map[monster_type](x, y, int(level)))
            nb_target = int(file.readline())
            self.targets = []
            for i in range(nb_target):
                [monster_type, level, grid_x, grid_y] = file.readline().split()
                x = int(grid_x)*background_img_size + background_img_size//2
                y = int(grid_y)*background_img_size + background_img_size//2
                if monster_type in monsters_map.keys():
                    self.targets.append(monsters_map[monster_type](x, y, int(level)))

    def draw_foes(self, frame):
        """Draw all foes on the window

        frame -- pygame.Surface, the window to draw on"""
        for target in self.targets:
            target.draw(frame)
        for enemy in self.enemies:
            enemy.draw(frame)

    def draw_board(self, frame):
        """Draw the board on the window

        frame -- pygame.Surface, the window to draw on"""
        self.board.draw(frame)

    def draw(self, frame, player, font, stage_index):
        """Draw ally units, foe units, the board, and the stage number on the window

        frame -- pygame.Surface, the window to draw on
        player -- the Player
        font -- pygame.font to write things on the window
        stage_index -- integer which stores the number of the current stage"""
        frame.fill((0, 0, 0))
        text = font.render("Stage " + str(stage_index), True, (255, 255, 255))
        frame.blit(text, (10, self.board.grid_height * background_img_size + 10))
        self.draw_board(frame)
        self.draw_foes(frame)
        for unit in player.alives:
            unit.draw(frame)
        for projectile in self.projectiles:
            projectile.draw(frame)
        pygame.display.update()

    def draw_initial_phase(self, frame, player, font, stage_index):
        """Draw the board during initial phase. We need another function, otherwise
        pygame.display.update is called twice

        frame -- pygame.Surface, the window to draw on
        player -- the Player
        font -- pygame.font to write things on the window
        stage_index -- integer which stores the number of the current stage"""
        frame.fill((0, 0, 0))
        text = font.render("Stage " + str(stage_index), True, (255, 255, 255))
        frame.blit(text, (10, self.board.grid_height * background_img_size + 10))
        self.draw_board(frame)
        self.draw_foes(frame)
        for unit in player.alives:
            unit.draw(frame)
        #for projectile in self.projectiles: # no projectile in initial phase
        #    projectile.draw(frame)

    def is_complete(self):
        """Check if there are remaining targets to kill"""
        return len(self.targets) == 0

    def check_death(self):
        """Remove dead foes from the lists"""
        for unit in (self.enemies + self.targets):
            if unit.current_health <= 0:
                unit.x = -1
                unit.y = -1     # prevent dead people from stopping projectiles
        self.enemies = [enemy for enemy in self.enemies if enemy.current_health > 0]
        self.targets = [target for target in self.targets if target.current_health > 0]

    def run_battle(self, player, frame, font, stage_index):
        """Start the battle

        player -- the Player
        frame -- pygame.Surface, the window to draw on
        font -- the pygame.Font for drawing text
        stage_index -- an integer for the current stage number"""
        clock = pygame.time.Clock()
        # battle loop
        while not self.is_complete() and not player.has_lost():
            clock.tick(60)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    return True

            for unit in player.alives:
                unit.action(pygame.Rect((0, 0, self.board.grid_width * background_img_size,
                                     self.board.grid_height * background_img_size)),
                        self, (self.enemies + self.targets), self.projectiles)

            for target in self.targets:
                target.action(pygame.Rect((0, 0, self.board.grid_width * background_img_size,
                                         self.board.grid_height * background_img_size)),
                            self, player.alives, self.projectiles)

            for enemy in self.enemies:
                enemy.action(pygame.Rect((0, 0, self.board.grid_width * background_img_size,
                                         self.board.grid_height * background_img_size)),
                            self, player.alives, self.projectiles)
            for projectile in self.projectiles:
                projectile.move()
            self.projectiles = [projectile for projectile in self.projectiles if not projectile.has_hit]

            player.check_death()
            self.check_death()

            self.draw(frame, player, font, stage_index)
        return False

    def valid_position(self, mouse_pos, player_units):
        """Check if a character can be put at the aimed position

        mouse_pos -- list of integer pixel mouse coordinates
        player_units -- list of Unit which contains units placed by the player """
        aimed_tile = self.board.pixels_to_tile(mouse_pos[0], mouse_pos[1])
        if self.board.grid[aimed_tile[0]][aimed_tile[1]] == Tile.FLOOR:
            for unit in player_units + self.targets + self.enemies:
                unit_tile = self.board.pixels_to_tile(unit.x, unit.y)
                if aimed_tile == unit_tile:
                    return False
            return True
        else:
            return False

    def initial_phase(self, player, frame, font, stage_index):
        """Handle the phase before battle when player can place units

        player -- the Player
        frame -- pygame.Surface, the window to draw on
        font -- the pygame.Font for drawing text
        stage_index -- an integer for the current stage number"""
        finished = False
        start_button = pygame.Rect((self.board.grid_width*background_img_size + 25), self.board.grid_height*background_img_size + 10, 150, 40)
        start_button_text = font.render("Battle !" , True, (255, 255, 255))

        stock = list(set(player.team) - set(player.alives))
        unit_buttons = []
        unit_button_width = 112
        unit_button_height = 64
        buttons_left_offset = 50
        between_buttons_offset = 15
        button_border = 5
        for i, unit in enumerate(stock):
            unit_buttons.append([unit,
                                 pygame.Rect((self.board.grid_width*background_img_size + buttons_left_offset),
                                             i * (background_img_size + between_buttons_offset) + between_buttons_offset, unit_button_width, unit_button_height)])
        unit_clicked = None
        clock = pygame.time.Clock()
        while not finished:
            clock.tick(60)
            mouse_pos = pygame.mouse.get_pos()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    return True
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == LEFT_CLICK:
                        mouse_pos = event.pos
                        if start_button.collidepoint(mouse_pos):
                            finished = True

                        if unit_clicked is None:
                            for unit_button in unit_buttons:
                                if unit_button[1].collidepoint(mouse_pos):
                                    unit_clicked = unit_button[0]
                            if unit_clicked is None:
                                for unit in player.alives:
                                    if unit.x - background_img_size//2 <= mouse_pos[0] <= unit.x + background_img_size//2 \
                                        and unit.y - background_img_size//2 <= mouse_pos[1] <= unit.y + background_img_size//2:
                                        unit_clicked = unit
                                        unit_buttons.append([unit,
                                                             pygame.Rect((self.board.grid_width*background_img_size + buttons_left_offset),
                                                            len(unit_buttons) * (background_img_size + between_buttons_offset) + between_buttons_offset, unit_button_width, unit_button_height)])
                                        player.alives = [alive_unit for alive_unit in player.alives if alive_unit is not unit_clicked]
                        else:
                            if 0 <= mouse_pos[0] < self.board.grid_width * background_img_size \
                                and 0 <= mouse_pos[1]  < self.board.grid_height * background_img_size:
                                unit_clicked.x = self.board.pixels_to_tile(unit_clicked.x, unit_clicked.y)[0] \
                                                 * background_img_size + background_img_size // 2
                                unit_clicked.y = self.board.pixels_to_tile(unit_clicked.x, unit_clicked.y)[1] \
                                                 * background_img_size + background_img_size // 2
                                if self.valid_position(mouse_pos,player.alives):
                                    player.alives.append(unit_clicked)
                                    unit_buttons = [unit_button for unit_button in unit_buttons
                                                    if unit_button[0] is not unit_clicked]
                                    unit_clicked = None
                                    stock = list(set(player.team) - set(player.alives))

                    elif event.button == RIGHT_CLICK:
                        if unit_clicked is not None:
                            unit_clicked = None

            self.draw_initial_phase(frame, player, font, stage_index)
            pygame.draw.rect(frame, (0, 255, 0), start_button)
            frame.blit(start_button_text, (start_button.x+8,start_button.y+5))
            for i, unit_button in enumerate(unit_buttons):
                unit_button[1].topleft = (self.board.grid_width * background_img_size + buttons_left_offset,
                            i * (background_img_size + between_buttons_offset) + between_buttons_offset)
                border = pygame.Rect(unit_button[1].x-button_border,
                                     unit_button[1].y-button_border,
                                     unit_button_width + 2*button_border,
                                     unit_button_height + 2*button_border)
                pygame.draw.rect(frame, (255, 255, 0), border)
                pygame.draw.rect(frame, (200, 200, 200), unit_button[1])
                image_to_display = unit_button[0].images[(State.IDLE, Direction.LEFT)][0]
                frame.blit(image_to_display,
                           (self.board.grid_width * background_img_size + buttons_left_offset + (112 - image_to_display.get_rect().width)//2,
                            i * (background_img_size + between_buttons_offset) + between_buttons_offset))
            if unit_clicked is not None:
                unit_clicked.x = mouse_pos[0]
                unit_clicked.y = mouse_pos[1]
                unit_clicked.draw(frame)
            pygame.display.update()
        return False
