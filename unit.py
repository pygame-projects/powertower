import pygame
import os
import random
from direction import Direction
from state import State
from numpy.linalg import norm

bar_length = 50
health_bar_height = 10
background_img_size = 64

images = {(State.IDLE, Direction.RIGHT): [pygame.transform.scale(
            pygame.image.load(os.path.join("assets/used/unit/chort_idle_anim_f" + str(i) + ".png")),
            (background_img_size, background_img_size)) for i in range(4)],
          (State.IDLE, Direction.LEFT): [pygame.transform.flip(pygame.transform.scale(
            pygame.image.load(os.path.join("assets/used/unit/chort_idle_anim_f" + str(i) + ".png")),
            (background_img_size, background_img_size)), True, False) for i in range(4)],
          (State.RUN, Direction.RIGHT): [pygame.transform.scale(
            pygame.image.load(os.path.join("assets/used/unit/chort_run_anim_f" + str(i) + ".png")),
            (background_img_size, background_img_size)) for i in range(4)],
          (State.RUN, Direction.LEFT): [pygame.transform.flip(pygame.transform.scale(
            pygame.image.load(os.path.join("assets/used/unit/chort_run_anim_f" + str(i) + ".png")),
            (background_img_size, background_img_size)), True, False) for i in range(4)],
          (State.ATTACK, Direction.RIGHT): [pygame.transform.scale(
            pygame.image.load(os.path.join("assets/used/unit/chort_idle_anim_f" + str(i) + ".png")),
            (background_img_size, background_img_size)) for i in range(4)],
          (State.ATTACK, Direction.LEFT): [pygame.transform.flip(pygame.transform.scale(
            pygame.image.load(os.path.join("assets/used/unit/chort_idle_anim_f" + str(i) + ".png")),
            (background_img_size, background_img_size)), True, False) for i in range(4)],
         }


class Unit:
    """Generic class for units"""
    def __init__(self, initial_x, initial_y, speed, attack, max_health):
        """Creates an unit

        initial_x -- an integer for the x initial coordinate
        initial_y -- an integer for the y initial coordinate
        speed -- an integer for the speed of the unit
        attack -- an Attack
        max_health -- an integer for the max_health of the unit"""
        self.x = initial_x
        self.y = initial_y
        self.path = []
        self.destination = []
        self.max_health = max_health
        self.current_health = max_health
        self.speed = speed
        self.attack = attack
        self.images = dict(images)
        self.current_images_index = 0
        self.animation_count = 0
        self.direction = Direction.RIGHT
        self.state = State.IDLE

    def draw(self, frame):
        """Draw the unit on the window

        frame -- pygame.Surface, the window to draw on"""
        current_image = self.images[(self.state, self.direction)][self.current_images_index]
        frame.blit(current_image, (self.x - current_image.get_width()//2, self.y - current_image.get_height()//2))
        self.draw_health(frame)

    def draw_health(self, frame):
        """Draw the health bar above the unit on the window

        frame -- pygame.Surface, the window to draw on"""
        health_length = (bar_length * self.current_health) // self.max_health
        pygame.draw.rect(frame, (255, 0, 0), (self.x - bar_length//2, self.y - background_img_size//2 - 5, bar_length, health_bar_height), 0)
        pygame.draw.rect(frame, (0, 255, 0), (self.x - bar_length//2, self.y - background_img_size//2 - 5, health_length, health_bar_height), 0)

    def can_move(self, direction, play_area, board):
        """Check if the unit can move to the given direction

        direction -- a Direction, where the unit wants to go
        play_area -- a pygame.Rect for the board limits
        board -- the Board, to look out for walls"""
        if direction == Direction.RIGHT:
            return self.x + self.speed < play_area.right and not board.is_wall(self.x + self.speed, self.y)
        elif direction == Direction.LEFT:
            return self.x - self.speed > play_area.left and not board.is_wall(self.x - self.speed, self.y)
        elif direction == Direction.UP:
            return self.y + self.speed < play_area.bottom and not board.is_wall(self.x, self.y + self.speed)
        elif direction == Direction.DOWN:
            return self.y - self.speed > play_area.top and not board.is_wall(self.x, self.y - self.speed)

    def move(self, play_area, board, opponents):
        """Choose a goal, and go towards it

        play_area -- a pygame.Rect for the board limits
        board -- the Board, to look out for walls
        opponents -- list of Unit, to choose a goal"""
        self.state = State.IDLE
        if len(self.destination) > 0 and max([abs(self.x - self.destination[0]), abs(self.y - self.destination[1])]) > self.speed:
            if self.x < self.destination[0] - self.speed and self.can_move(Direction.RIGHT, play_area, board):
                self.state = State.RUN
                self.direction = Direction.RIGHT
                self.x += self.speed
            elif self.x > self.destination[0] + self.speed and self.can_move(Direction.LEFT, play_area, board):
                self.state = State.RUN
                self.direction = Direction.LEFT
                self.x -= self.speed

            if self.y < self.destination[1] - self.speed and self.can_move(Direction.DOWN, play_area, board):
                self.state = State.RUN
                self.y += self.speed
            elif self.y > self.destination[1] + self.speed and self.can_move(Direction.UP, play_area, board):
                self.state = State.RUN
                self.y -= self.speed

        else:
            goal = self.find_goal(opponents)
            self.path = board.shortest_path(board.pixels_to_tile(self.x, self.y),
                                            board.pixels_to_tile(goal[0], goal[1]))
            if len(self.path) > 0:
                self.destination = self.path.pop(0)

    def find_goal(self, opponents):
        """Return the coordinates of the nearest opponent

        opponents -- list of Unit"""
        min_dist = -1
        x = self.x
        y = self.y
        for foe in opponents:
            dist = norm([foe.x - self.x, foe.y - self.y])
            if (dist < min_dist) or min_dist == -1:
                x = foe.x
                y = foe.y
                min_dist = dist
        return [x, y]

    def enemy_near(self, opponents):
        """Look for an enemy enough close to hit and return it"""
        for foe in opponents:
            if norm([foe.x - self.x, foe.y - self.y]) <= self.attack.reach:
                return foe
        return None

    def action(self, play_area, stage, opponents, projectiles):
        """Choose between moving or attacking

        play_area -- a pygame.Rect for the board limits
        stage -- the Stage
        opponents -- list of Unit
        projectiles -- list of Projectile, where must be added thrown projectiles"""
        if self.state == State.ATTACK:
            self.current_images_index += 1
            if self.current_images_index >= len(self.images[(State.ATTACK, self.direction)]):
                self.state = State.IDLE
                self.current_images_index = 0
        else:
            self.animation_count += 1
            if self.attack.count > 0:
                self.attack.count -= 1
            if self.animation_count >= 45 // len(self.images[(self.state, self.direction)]):
                self.animation_count = 0
                self.current_images_index += 1
            if self.current_images_index >= len(self.images[(self.state, self.direction)]):
                self.current_images_index = 0
            nearest_enemy = self.enemy_near(opponents)
            if nearest_enemy is None:
                self.move(play_area, stage.board, opponents)
            else:
                self.current_images_index = 0
                if self.attack.count == 0:
                    self.state = State.ATTACK
                    self.attack.hit(self.x, self.y, nearest_enemy.x, nearest_enemy.y, opponents, projectiles)
