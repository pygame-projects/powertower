from attack import Attack

base_range = 32
base_power = 1
base_area = 32
base_cooldown = 45


class WeakMeleeAttack(Attack):
    """A weak type of melee attack, which deals less damages but have less cooldown"""
    def __init__(self, level):
        """Creates a weak melee attack instance
        level -- the level of the attack"""
        super().__init__(base_power, base_range, base_area, level, base_cooldown)

    def level_up(self):
        """Increase the level of the attack"""
        self.level += 1
