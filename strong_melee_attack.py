from attack import Attack

base_range = 32
base_power = 6
base_area = 32
base_cooldown = 90


class StrongMeleeAttack(Attack):
    """A strong melee attack, which deals more damages but is slower"""
    def __init__(self, level):
        """Creates a strong melee attack instance
        level -- the level of the attack"""
        super().__init__(base_power, base_range, base_area, level, base_cooldown)

    def level_up(self):
        """Increase the level of the attack"""
        self.level += 1
