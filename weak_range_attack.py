from range_attack import RangeAttack

base_range = 300
base_power = 1
base_area = 40
base_cooldown = 30
base_speed = 7


class WeakRangeAttack(RangeAttack):
    """A weak type of ranged attack, which deals less damages but have less cooldown"""

    def __init__(self, level):
        """Creates a weak ranged attack instance
        level -- the level of the attack"""
        super().__init__(base_power, base_range, base_area,level, base_cooldown, base_speed)

    def level_up(self):
        """Increase the level of the attack"""
        self.level += 1
