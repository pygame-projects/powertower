from enum import Enum


class State(Enum):
    """Enum for different animation states of an unit"""
    IDLE = 1
    RUN = 2
    ATTACK = 3
