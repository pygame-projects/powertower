from enum import Enum


class Tile(Enum):
    """Enum for different available kinds of tiles on the board"""
    FLOOR = 1
    WALL = 2
