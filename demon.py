import pygame
import os
from unit import Unit
from weak_range_attack import WeakRangeAttack
from direction import Direction
from state import State

background_img_size = 64
base_health = 2
base_speed = 5


class Demon(Unit):
    """Small demon, fast and squishy ranged monster"""

    def __init__(self, initial_x, initial_y, level):
        """Creates a Demon

        initial_x -- integer for the initial x coordinate
        initial_y -- integer for the initial y coordinate
        level -- integer for the unit level"""
        super().__init__(initial_x, initial_y, base_speed, WeakRangeAttack(level), base_health)
        self.level = level
