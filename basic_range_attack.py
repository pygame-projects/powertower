from range_attack import RangeAttack
base_range = 300
base_power = 2
base_area = 40
base_cooldown = 90
base_speed = 7


class BasicRangeAttack(RangeAttack):
    """A regular type of range attack"""

    def __init__(self, level):
        """Creates an instance of BasicRangeAttack

        level -- the level of the attack"""
        super().__init__(base_power, base_range, base_area, level, base_cooldown, base_speed)

    def level_up(self):
        """Increase the level of the attack"""
        self.level += 1
