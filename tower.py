from stage import Stage


class Tower:
    """A class which represents a tower, which is a group of stages"""
    def __init__(self, file_name):
        """Creates a tower from a file

        file_name -- a string for the name of the file to open"""
        self.stages = []
        with open(file_name, "r") as file:
            self.nb_stages = int(file.readline())
            for i in range(self.nb_stages):
                stage_file = file.readline()
                self.stages.append(Stage(stage_file.rstrip("\n")))
        self.current_stage_index = 0

    def get_current_stage(self):
        """Return the current Stage"""
        return self.stages[self.current_stage_index]

    def has_next_stage(self):
        """Check if there are remaining stages after the current stage"""
        return self.current_stage_index < (self.nb_stages-1)

    def go_to_next_stage(self):
        """Change the current stage to the next stage"""
        if self.has_next_stage():
            self.current_stage_index += 1
