from attack import Attack
from projectile import Projectile


class RangeAttack(Attack):
    """Generic class for the ranged attack behaviour"""
    def __init__(self, base_power, base_range, base_area, level, base_cooldown, speed):
        """Instantiate a ranged attack

        base_power -- an integer for the damages of the attack
        base_range -- an integer for the range of the trigger of the attack
        base_area -- an integer for the range of the damages when the attack hits
        level -- an integer for the level of the attack
        base_cooldown -- an integer which represents the number of frames to wait before re-attack
        speed -- an integer for the speed of the summoned projectile

        count is an integer attribute which handles the attack cooldown"""
        super().__init__(base_power, base_range, base_area,level, base_cooldown)
        self.speed = speed

    def hit(self, attacker_x, attacker_y, aimed_x, aimed_y, unit_list, projectiles):
        """Summon a projectile which will goes towards the aimed point

         attacker_x -- an integer for the x coordinate of the attacker
         attacker_y -- an integer for the y coordinate of the attacker
         aimed_x -- an integer for the x coordinate of the aimed point
         aimed_x -- an integer for the y coordinate of the aimed point
         unit_list -- a list of Unit,  potential targets for the attack
         projectiles -- list of Projectile"""
        direction_vector = [aimed_x - attacker_x, aimed_y - attacker_y]
        projectiles.append(Projectile(attacker_x, attacker_y, direction_vector,
            self.speed, self.power, self.effect_area, unit_list))
        self.count = self.cooldown

    def level_up(self):
        pass
