import pygame
import os
from unit import Unit
from basic_melee_attack import BasicMeleeAttack
from direction import Direction
from state import State

background_img_size = 64
attack_img_size = 112

images = {(State.IDLE, Direction.RIGHT): [pygame.transform.scale(
            pygame.image.load(os.path.join("assets/used/melee_unit/knight_m_idle_anim_f" + str(i) + ".png")),
            (background_img_size, background_img_size)) for i in range(4)],
          (State.IDLE, Direction.LEFT): [pygame.transform.flip(pygame.transform.scale(
            pygame.image.load(os.path.join("assets/used/melee_unit/knight_m_idle_anim_f" + str(i) + ".png")),
            (background_img_size, background_img_size)), True, False) for i in range(4)],
          (State.RUN, Direction.RIGHT): [pygame.transform.scale(
            pygame.image.load(os.path.join("assets/used/melee_unit/knight_m_idle_anim_f" + str(i) + ".png")),
            (background_img_size, background_img_size)) for i in range(4)],
          (State.RUN, Direction.LEFT): [pygame.transform.flip(pygame.transform.scale(
            pygame.image.load(os.path.join("assets/used/melee_unit/knight_m_idle_anim_f" + str(i) +".png")),
            (background_img_size, background_img_size)), True, False) for i in range(4)],
          (State.ATTACK, Direction.RIGHT): [pygame.transform.scale(
            pygame.image.load(os.path.join("assets/used/melee_unit/knight_m_attack_anim_f" + str(i) + ".png")),
            (attack_img_size, background_img_size)) for i in range(4)],
          (State.ATTACK, Direction.LEFT): [pygame.transform.flip(pygame.transform.scale(
            pygame.image.load(os.path.join("assets/used/melee_unit/knight_m_attack_anim_f" + str(i) + ".png")),
            (attack_img_size, background_img_size)), True, False) for i in range(4)]
          }

base_health = 10
base_speed = 3


class MeleeUnit(Unit):
    """Knight, polyvalent ally melee unit"""

    def __init__(self, initial_x, initial_y, level):
        """Creates a MeleeUnit

        initial_x -- integer for the initial x coordinate
        initial_y -- integer for the initial y coordinate
        level -- integer for the unit level"""
        super().__init__(initial_x, initial_y, base_speed, BasicMeleeAttack(level), base_health)
        self.level = level
        self.images = dict(images)

    def level_up(self):
        """Increase the level of the unit"""
        self.attack.level_up()
        self.level += 1
