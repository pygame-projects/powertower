import pygame
import os
from unit import Unit
from weak_melee_attack import WeakMeleeAttack
from direction import Direction
from state import State

background_img_size = 64

images = {(State.IDLE, Direction.RIGHT): [pygame.transform.scale(
            pygame.image.load(os.path.join("assets/used/goblin/goblin_idle_anim_f" + str(i) + ".png")),
            (background_img_size, background_img_size)) for i in range(4)],
          (State.IDLE, Direction.LEFT): [pygame.transform.flip(pygame.transform.scale(
            pygame.image.load(os.path.join("assets/used/goblin/goblin_idle_anim_f" + str(i) + ".png")),
            (background_img_size, background_img_size)), True, False) for i in range(4)],
          (State.RUN, Direction.RIGHT): [pygame.transform.scale(
            pygame.image.load(os.path.join("assets/used/goblin/goblin_idle_anim_f" + str(i) + ".png")),
            (background_img_size, background_img_size)) for i in range(4)],
          (State.RUN, Direction.LEFT): [pygame.transform.flip(pygame.transform.scale(
            pygame.image.load(os.path.join("assets/used/goblin/goblin_idle_anim_f" + str(i) +".png")),
            (background_img_size, background_img_size)), True, False) for i in range(4)],
          # TODO animations à changer
          (State.ATTACK, Direction.RIGHT): [pygame.transform.scale(
            pygame.image.load(os.path.join("assets/used/goblin/goblin_idle_anim_f" + str(i) + ".png")),
            (background_img_size, background_img_size)) for i in range(4)],
          (State.ATTACK, Direction.LEFT): [pygame.transform.flip(pygame.transform.scale(
            pygame.image.load(os.path.join("assets/used/goblin/goblin_idle_anim_f" + str(i) + ".png")),
            (background_img_size, background_img_size)), True, False) for i in range(4)]
          }

base_health = 3
base_speed = 5


class Goblin(Unit):
    """Goblin unit, fast and squishy melee monster"""
    def __init__(self, initial_x, initial_y, level):
        """Creates a Goblin

        initial_x -- integer for the initial x coordinate
        initial_y -- integer for the initial y coordinate
        level -- integer for the unit level"""
        super().__init__(initial_x, initial_y, base_speed, WeakMeleeAttack(level), base_health)
        self.level = level
        self.images = dict(images)
